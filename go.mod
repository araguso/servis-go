module gitlab.ethz.ch/araguso/servis-go

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/mwitkow/go-proto-validators v0.3.2
	google.golang.org/genproto v0.0.0-20211013025323-ce878158c4d4
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
