// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package dns

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// DnsClient is the client API for Dns service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DnsClient interface {
	// CreateARecord CreateARecord
	CreateARecord(ctx context.Context, in *CreateARecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error)
	// DeleteARecord DeleteARecord
	DeleteARecord(ctx context.Context, in *DeleteARecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error)
	// CreateCNameRecord CreateCNameRecord
	CreateCNameRecord(ctx context.Context, in *CreateCNameRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error)
	// DeleteCNameRecord DeleteCNameRecord
	DeleteCNameRecord(ctx context.Context, in *DeleteCNameRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error)
	// CreateTxtRecord CreateTxtRecord
	CreateTxtRecord(ctx context.Context, in *CreateTxtRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error)
	// SearchTxtRecord SearchTxtRecord
	// Does not return information about the ISG group the record belongs to, because why not.
	SearchTxtRecord(ctx context.Context, in *SearchTxtRecordRequest, opts ...grpc.CallOption) (*TxtResponse, error)
	// DeleteTxtRecord DeleteTxtRecord
	DeleteTxtRecord(ctx context.Context, in *DeleteTxtRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error)
}

type dnsClient struct {
	cc grpc.ClientConnInterface
}

func NewDnsClient(cc grpc.ClientConnInterface) DnsClient {
	return &dnsClient{cc}
}

func (c *dnsClient) CreateARecord(ctx context.Context, in *CreateARecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error) {
	out := new(EmptyResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.dns.Dns/CreateARecord", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dnsClient) DeleteARecord(ctx context.Context, in *DeleteARecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error) {
	out := new(EmptyResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.dns.Dns/DeleteARecord", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dnsClient) CreateCNameRecord(ctx context.Context, in *CreateCNameRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error) {
	out := new(EmptyResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.dns.Dns/CreateCNameRecord", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dnsClient) DeleteCNameRecord(ctx context.Context, in *DeleteCNameRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error) {
	out := new(EmptyResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.dns.Dns/DeleteCNameRecord", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dnsClient) CreateTxtRecord(ctx context.Context, in *CreateTxtRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error) {
	out := new(EmptyResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.dns.Dns/CreateTxtRecord", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dnsClient) SearchTxtRecord(ctx context.Context, in *SearchTxtRecordRequest, opts ...grpc.CallOption) (*TxtResponse, error) {
	out := new(TxtResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.dns.Dns/SearchTxtRecord", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dnsClient) DeleteTxtRecord(ctx context.Context, in *DeleteTxtRecordRequest, opts ...grpc.CallOption) (*EmptyResponse, error) {
	out := new(EmptyResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.dns.Dns/DeleteTxtRecord", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DnsServer is the server API for Dns service.
// All implementations must embed UnimplementedDnsServer
// for forward compatibility
type DnsServer interface {
	// CreateARecord CreateARecord
	CreateARecord(context.Context, *CreateARecordRequest) (*EmptyResponse, error)
	// DeleteARecord DeleteARecord
	DeleteARecord(context.Context, *DeleteARecordRequest) (*EmptyResponse, error)
	// CreateCNameRecord CreateCNameRecord
	CreateCNameRecord(context.Context, *CreateCNameRecordRequest) (*EmptyResponse, error)
	// DeleteCNameRecord DeleteCNameRecord
	DeleteCNameRecord(context.Context, *DeleteCNameRecordRequest) (*EmptyResponse, error)
	// CreateTxtRecord CreateTxtRecord
	CreateTxtRecord(context.Context, *CreateTxtRecordRequest) (*EmptyResponse, error)
	// SearchTxtRecord SearchTxtRecord
	// Does not return information about the ISG group the record belongs to, because why not.
	SearchTxtRecord(context.Context, *SearchTxtRecordRequest) (*TxtResponse, error)
	// DeleteTxtRecord DeleteTxtRecord
	DeleteTxtRecord(context.Context, *DeleteTxtRecordRequest) (*EmptyResponse, error)
	mustEmbedUnimplementedDnsServer()
}

// UnimplementedDnsServer must be embedded to have forward compatible implementations.
type UnimplementedDnsServer struct {
}

func (UnimplementedDnsServer) CreateARecord(context.Context, *CreateARecordRequest) (*EmptyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateARecord not implemented")
}
func (UnimplementedDnsServer) DeleteARecord(context.Context, *DeleteARecordRequest) (*EmptyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteARecord not implemented")
}
func (UnimplementedDnsServer) CreateCNameRecord(context.Context, *CreateCNameRecordRequest) (*EmptyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateCNameRecord not implemented")
}
func (UnimplementedDnsServer) DeleteCNameRecord(context.Context, *DeleteCNameRecordRequest) (*EmptyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteCNameRecord not implemented")
}
func (UnimplementedDnsServer) CreateTxtRecord(context.Context, *CreateTxtRecordRequest) (*EmptyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateTxtRecord not implemented")
}
func (UnimplementedDnsServer) SearchTxtRecord(context.Context, *SearchTxtRecordRequest) (*TxtResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchTxtRecord not implemented")
}
func (UnimplementedDnsServer) DeleteTxtRecord(context.Context, *DeleteTxtRecordRequest) (*EmptyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteTxtRecord not implemented")
}
func (UnimplementedDnsServer) mustEmbedUnimplementedDnsServer() {}

// UnsafeDnsServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DnsServer will
// result in compilation errors.
type UnsafeDnsServer interface {
	mustEmbedUnimplementedDnsServer()
}

func RegisterDnsServer(s grpc.ServiceRegistrar, srv DnsServer) {
	s.RegisterService(&Dns_ServiceDesc, srv)
}

func _Dns_CreateARecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateARecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DnsServer).CreateARecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.dns.Dns/CreateARecord",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DnsServer).CreateARecord(ctx, req.(*CreateARecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dns_DeleteARecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteARecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DnsServer).DeleteARecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.dns.Dns/DeleteARecord",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DnsServer).DeleteARecord(ctx, req.(*DeleteARecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dns_CreateCNameRecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateCNameRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DnsServer).CreateCNameRecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.dns.Dns/CreateCNameRecord",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DnsServer).CreateCNameRecord(ctx, req.(*CreateCNameRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dns_DeleteCNameRecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteCNameRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DnsServer).DeleteCNameRecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.dns.Dns/DeleteCNameRecord",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DnsServer).DeleteCNameRecord(ctx, req.(*DeleteCNameRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dns_CreateTxtRecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateTxtRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DnsServer).CreateTxtRecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.dns.Dns/CreateTxtRecord",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DnsServer).CreateTxtRecord(ctx, req.(*CreateTxtRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dns_SearchTxtRecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchTxtRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DnsServer).SearchTxtRecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.dns.Dns/SearchTxtRecord",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DnsServer).SearchTxtRecord(ctx, req.(*SearchTxtRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dns_DeleteTxtRecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteTxtRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DnsServer).DeleteTxtRecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.dns.Dns/DeleteTxtRecord",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DnsServer).DeleteTxtRecord(ctx, req.(*DeleteTxtRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Dns_ServiceDesc is the grpc.ServiceDesc for Dns service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Dns_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "vseth.sip.dns.Dns",
	HandlerType: (*DnsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateARecord",
			Handler:    _Dns_CreateARecord_Handler,
		},
		{
			MethodName: "DeleteARecord",
			Handler:    _Dns_DeleteARecord_Handler,
		},
		{
			MethodName: "CreateCNameRecord",
			Handler:    _Dns_CreateCNameRecord_Handler,
		},
		{
			MethodName: "DeleteCNameRecord",
			Handler:    _Dns_DeleteCNameRecord_Handler,
		},
		{
			MethodName: "CreateTxtRecord",
			Handler:    _Dns_CreateTxtRecord_Handler,
		},
		{
			MethodName: "SearchTxtRecord",
			Handler:    _Dns_SearchTxtRecord_Handler,
		},
		{
			MethodName: "DeleteTxtRecord",
			Handler:    _Dns_DeleteTxtRecord_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "vseth/sip/dns/dns.proto",
}
